import React from 'react';
import {  BrowserRouter, Routes, Route} from 'react-router-dom';
import LandingPage from './pages/LandingPage/LandingPage';
import InitiatePlayQuiz from './pages/InitiatePlayQuiz/InitiatePlayQuiz';
import CreateQuiz from './pages/CreateQuiz/CreateQuiz'
import InitiateCreateQuiz from './pages/InitiateCreateQuiz/InitiateCreateQuiz';
import PlayQuiz from './pages/PlayQuiz/PlayQuiz';
import Leaderboard from './pages/Leaderboard/Leaderboard';
import LoginPage from './pages/Auth/LoginPage';
import RegisterPage from './pages/Auth/RegisterPage';
function App() {
  
  return (
    <BrowserRouter>
    <Routes>
        <Route path="/" element={<LandingPage />}> </Route>
        <Route path="/create_quiz" element={<CreateQuiz />}> </Route>
        <Route path='/play_quiz/initiate' element={<InitiatePlayQuiz/>}> </Route>
        <Route path="/create_quiz/initiate" element={<InitiateCreateQuiz />}>  </Route>  
        <Route path='/play_quiz' element={<PlayQuiz/>}> </Route>
        <Route path='/leaderboard' element={<Leaderboard/>}> </Route>
        <Route path='/auth/login/play' element={<LoginPage action="play"/>}> </Route>
        <Route path='/auth/login/create' element={<LoginPage action="create"/>}> </Route>
        <Route path='/auth/register' element={<RegisterPage/>}></Route>
        <Route path='/create_quiz/launch' element={<CreateQuiz/>}></Route>
        
    </Routes>
  </BrowserRouter>
  );
}

export default App;

// (new Date(Date.now() + 60000)).toISOString();
