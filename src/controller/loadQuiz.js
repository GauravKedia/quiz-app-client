import axios from "axios";
import apiPaths from "../api";

const loadQuiz = async (quizName,questionNumber) =>{
    try{
        const accessToken = localStorage.getItem('accessToken');
        const refreshToken = localStorage.getItem('refreshToken');
        if(accessToken && refreshToken){
            const headers = {
                'Authorization': `Bearer ${accessToken} ${refreshToken}`
            };
            console.log("Bye Bye Frontend");
            console.log(apiPaths.loadQuiz);
            const loadQuizResponse =  await axios.post(apiPaths.loadQuiz,{quizName,questionNumber},{headers:headers});
            console.log("Load Quiz Response: "+loadQuizResponse);
            return loadQuizResponse.data;
        }
        else{
            window.alert("Please Login Again");
            return;
        }  
    }
    catch(error){
        throw new Error(error);
    }
}

export {loadQuiz};