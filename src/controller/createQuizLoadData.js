import axios from "axios";
import apiPaths from "../api";

const createQuizLoadData = async (quizName) =>{
    try{
        // console.log("Went");
        const accessToken = localStorage.getItem('accessToken');
        const refreshToken = localStorage.getItem('refreshToken');    
        // console.log("Quiz Name   : "+quizName);
        if(accessToken && refreshToken){
            const headers = {
                'Authorization': `Bearer ${accessToken} ${refreshToken}`
            };
        console.log("api :" +apiPaths.createQuizStartLaunch);
        const createQuizStartLaunchResponse = await axios.post(apiPaths.createQuizLoadData,{quizName:quizName},{headers:headers});
        // console.log(" createQuizStartLaunchResponse : "+createQuizStartLaunchResponse);
        return createQuizStartLaunchResponse.data;
        }
        else{
            window.alert("Please Login Again");
        }
    }
    catch(error){
        throw new Error(error.login_response.data.message);
    }
}

export {createQuizLoadData};