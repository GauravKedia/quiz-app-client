import axios from "axios";
import apiPaths from "../../api";

const playQuizLoadData = async (quizName, setUserName, setCurrentScore) => {
  try {

    console.log("Play Quiz Load Data Received : " +quizName +" : " +setUserName +" : " +setCurrentScore);

    const accessToken = localStorage.getItem("accessToken");
    const refreshToken = localStorage.getItem("refreshToken");
    if (accessToken && refreshToken) {
      const headers = {
        Authorization: `Bearer ${accessToken} ${refreshToken}`,
      };
    //   console.log("Play Quiz Load Data api :" + apiPaths.playQuizLoadData);
      const playQuizLoadDataResponse = await axios.post(
        apiPaths.playQuizLoadData,
        { quizName: quizName },
        { headers: headers }
      );
      if (playQuizLoadDataResponse && playQuizLoadDataResponse.data.message === "Play Quiz Data Found!") {
        
        console.log(
          "Play Quiz Load Data Recevied from Backend: " +
            playQuizLoadDataResponse.data.userName +
            " : " +
            playQuizLoadDataResponse.data.score
        );

        setUserName(playQuizLoadDataResponse.data.userName);
        setCurrentScore(playQuizLoadDataResponse.data.score);
      }
    } else {
      window.alert("Please Login Again");
    }
  } catch (error) {
    throw new Error(error.login_response.data.message);
  }
};

export default playQuizLoadData;
