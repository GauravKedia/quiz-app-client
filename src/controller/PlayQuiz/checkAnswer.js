import axios from "axios";
import apiPaths from "../../api";


const checkAnswer = async (optionValue, currentQNo,quizName) => {
  try {
    const accessToken = localStorage.getItem("accessToken");
    const refreshToken = localStorage.getItem("refreshToken");
    if (accessToken && refreshToken) {
      const headers = {
        Authorization: `Bearer ${accessToken} ${refreshToken}`,
      };
      console.log("Check Answer Response Sending Data to Backend : "+ quizName + " : " + optionValue + " : "+ currentQNo);
      console.log("Check Answer Backend Path : " + apiPaths.checkAnswer);
      const checkAnswerResponse = await axios.post(
        apiPaths.checkAnswer,
        { quizName: quizName,optionValue:optionValue,currentQNo:currentQNo},
        { headers: headers }
      );
      if(checkAnswerResponse){
        console.log("Check Answer: " + checkAnswerResponse.data.message);
        return checkAnswerResponse.data.message;
      }
      return;
    } else {
      window.alert("Please Login Again");
      return;
    }
  } catch (error) {
    throw new Error(error.login_response.data.message);
  }
};

export { checkAnswer };
