import axios from "axios";
import apiPaths from "../../api";


const leaderboard = async (quizName) => {
  try {
    const accessToken = localStorage.getItem("accessToken");
    const refreshToken = localStorage.getItem("refreshToken");
    
    if (accessToken && refreshToken) {
      const headers = {
        Authorization: `Bearer ${accessToken} ${refreshToken}`,
      };
      console.log("leaderboard sending : "+ quizName);
      const leaderboardResponse = await axios.post(
        apiPaths.leaderboard,
        { quizName: quizName},
        { headers: headers }
      );
      if(leaderboardResponse){
        console.log("leaderboard Recieved: " + leaderboardResponse.data);
        return leaderboardResponse.data;
      }
      return null;
    } else {
      window.alert("Please Login Again");
      return null;
    }
  } catch (error) {
    console.log("Controller: "+error.login_response.data.message);
  }
};

export { leaderboard };
