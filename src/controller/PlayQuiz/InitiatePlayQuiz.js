import axios from "axios";
import apiPaths from "../../api";


const initiatePlayQuiz = async (quizName, quizPin) => {
  try {
    const accessToken = localStorage.getItem("accessToken");
    const refreshToken = localStorage.getItem("refreshToken");

    // For Leaderboard
    localStorage.setItem('quizName', quizName);
    console.log("Initiate Play Quiz Stored QuizName : "+quizName)
    
    
    if (accessToken && refreshToken) {
      const headers = {
        Authorization: `Bearer ${accessToken} ${refreshToken}`,
      };
      console.log("Initiate Play Quiz going to backend: "+ quizName + "  : "+quizPin);
      const initiatePlayQuizResponse = await axios.post(
        apiPaths.initiatePlayQuiz,
        { quizName: quizName, quizPin: quizPin },
        { headers: headers }
      );
      if(initiatePlayQuizResponse){
        console.log("Initiate Playe QUIZ Recieved: " + initiatePlayQuizResponse.data.message);
        return initiatePlayQuizResponse.data.message;
      }
    } else {
      window.alert("Please Login Again");
      return;
    }
  } catch (error) {
    throw new Error(error.login_response.data.message);
  }
};

export { initiatePlayQuiz };
