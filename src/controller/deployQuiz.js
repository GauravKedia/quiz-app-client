import axios from "axios";
import apiPaths from "../api";

const deployQuiz = async (quizName,scheduleDate) =>{
    try{
        const accessToken = localStorage.getItem('accessToken');
        const refreshToken = localStorage.getItem('refreshToken');
        if(accessToken && refreshToken){
            const headers = {
                'Authorization': `Bearer ${accessToken} ${refreshToken}`
            };
            // console.log("Bye Bye Frontend");
            console.log("deploy :"+apiPaths.loadQuiz);
            await axios.post(apiPaths.deployQuiz,{quizName,scheduleDate},{headers:headers});
        }
        else{
            window.alert("Please Login Again");
            return;
        }  
    }
    catch(error){
        throw new Error(error);
    }
}

export {deployQuiz};