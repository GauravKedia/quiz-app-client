import axios from 'axios';
import apiPaths from '../api';

const initiateCreateQuiz = async (quizName,quizPin) =>{
    try{    
        const accessToken = localStorage.getItem('accessToken');
        const refreshToken = localStorage.getItem('refreshToken');
        if(accessToken && refreshToken){
            const headers = {
                'Authorization': `Bearer ${accessToken} ${refreshToken}`
            };
            const initiateCreateQuizResponse = await axios.post(apiPaths.initiateCreateQuiz,{quizName:quizName,quizPin:quizPin},{headers:headers});
            return initiateCreateQuizResponse.data.message;
        }
        else{
            window.alert("Please Login Again");
            return;
        }
    }
    catch(error){
        throw new Error(error.login_response.data.message);
    }
}

export {initiateCreateQuiz};