import axios from "axios";
import apiPaths from "../api";

const createQuizStartLaunch = async () =>{
    
    try{
        // console.log("Went");
        const accessToken = localStorage.getItem('accessToken');
        const refreshToken = localStorage.getItem('refreshToken');    
        // console.log(accessToken);
        if(accessToken && refreshToken){
            const headers = {
                'Authorization': `Bearer ${accessToken} ${refreshToken}`
            };
        // console.log("api :" +apiPaths.createQuizStartLaunch);
        const createQuizStartLaunchResponse = await axios.post(apiPaths.createQuizStartLaunch,{},{headers:headers});
        // console.log("Hi  : "+createQuizStartLaunchResponse);
        return createQuizStartLaunchResponse;
        }
        else{
            window.alert("Please Login Again");
        }
    }
    catch(error){
        throw new Error(error.login_response.data.message);
    }
}

export {createQuizStartLaunch};