import axios from "axios";
import apiPaths from "../api";

const saveCreateQuiz = async (quizData) =>{
    try{
        const accessToken = localStorage.getItem('accessToken');
        const refreshToken = localStorage.getItem('refreshToken');
        if(accessToken && refreshToken){
            const headers = {
                'Authorization': `Bearer ${accessToken} ${refreshToken}`
            };
            const saveQuizResponse =  await axios.post(apiPaths.saveCreateQuiz,{quizData},{headers:headers});
            console.log("Save Quiz Response: "+saveQuizResponse.data.message);
            
            if(saveQuizResponse.data.message ==="Question added to quiz successfully!"){
                window.alert("Question Saved");
            }
            return saveQuizResponse.data.message;
        }
        else{
            window.alert("Please Login Again");
            return;
        }  
    }
    catch(error){
        throw new Error(error);
    }
}

export {saveCreateQuiz};