// import env from "react-dotenv";
const API_Base_Url = "https://quiz-app-server-o1rp.onrender.com"; 
const Socket_Base_Url = "https://quiz-app-server-o1rp.onrender.com"; 

const apiPaths = {
  login: `${API_Base_Url}/auth/login`,
  register: `${API_Base_Url}/auth/register`,
  initiateCreateQuiz: `${API_Base_Url}/create_quiz/initiate`,
  createQuizStartLaunch: `${API_Base_Url}/create_quiz/startLaunch`,
  createQuizLoadData: `${API_Base_Url}/create_quiz/loadQuizData`,
  saveCreateQuiz: `${API_Base_Url}/create_quiz/saveQuiz`,
  loadQuiz: `${API_Base_Url}/create_quiz/loadQuiz`,
  deleteQuiz: `${API_Base_Url}/create_quiz/deleteQuiz`,
  deployQuiz: `${API_Base_Url}/create_quiz/deployQuiz`,
  initiatePlayQuiz: `${API_Base_Url}/play_quiz/initiate`,
  playQuizLoadData: `${API_Base_Url}/play_quiz/playQuizLoadData`,
  checkAnswer : `${API_Base_Url}/play_quiz/checkAnswer`,
  leaderboard: `${API_Base_Url}/play_quiz/leaderboard`,
  socketUrl : `${Socket_Base_Url}`,
};

export default apiPaths;
