import React, { useState, useEffect } from "react";
// import Profiles from "./profiles";
import "./Leaderboard.css";
import { useNavigate } from "react-router-dom";
import { leaderboard } from "../../controller/PlayQuiz/leaderboard";
import Loader from "../loader/loader";
const Leaderboard = () => {
  const [leaderboardData, setLeaderboardData] = useState([]);
  const [leaderboardLoading, setLeaderboardLoading] = useState(true);
  let quizName = localStorage.getItem("quizName");
  let navigate = useNavigate();

  useEffect(() => {
    quizName = localStorage.getItem("quizName");
    console.log("Leaderboard Page: QuizName " + quizName);
    const fetchLeaderboardData = async () => {
      try {
        const data = await leaderboard(quizName);
        console.log(data);
        if (data) {
          console.log("LeaderBoard data from backend : " + data);
          setLeaderboardData(data);
          setLeaderboardLoading(false);
        }
      } catch (error) {
        console.error("Error fetching leaderboard data: ", error);
      }
    };

    fetchLeaderboardData();
  }, []);

  const return_back = () => {
    let path = "/";
    navigate(path);
  };

  return (
    <div className="board">
      {leaderboardLoading ? (
        <Loader />
      ) : (
        <>
          <h1 className="leaderboard">Leaderboard</h1>
          <button onClick={return_back} className="leaderboard_close_button">
            Close
          </button>
          <div className="duration">{quizName}</div>

          <div id="profile">
            {leaderboardData.map((value, index) => (
              <div className="flex" key={index}>
                <div className="leaderboard_index">
                  <h2>{index + 1}.</h2>
                </div>
                <div className="item">
                  <div className="info">
                    <h3 className="name text-dark">{value.userName}</h3>
                  </div>
                </div>
                <div className="item">
                  <h2>
                    <span>{value.score}</span>
                  </h2>
                </div>
              </div>
            ))}
          </div>

        </>
      )}
    </div>
  );
};

export default Leaderboard;
