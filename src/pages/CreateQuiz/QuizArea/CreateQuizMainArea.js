import React, { useContext, useState, useEffect } from "react";
import "./CreateQuizMainArea.css";
import { saveCreateQuiz } from "../../../controller/saveCreateQuiz";
import { loadQuiz } from "../../../controller/loadQuiz";
import { deleteQuiz } from "../../../controller/deleteQuiz";
import { createQuizLoadData } from "../../../controller/createQuizLoadData";
// import {quizNameContext} from "../Navbar/CreateQuizNavbar"
const create_quiz_sidebar_quiz =
  process.env.PUBLIC_URL + "/images/create_quiz_sidebar_quiz.png";

function CreateQuizMainArea({ passQuizName }) {
  // console.log("Value of passQuizName Main:", passQuizName);
  const [questionValue, setQuestionValue] = useState("");
  const [options, setOptions] = useState(["", "", "", ""]);
  const [correctOption, setCorrectOption] = useState(1);
  const [points, setPoints] = useState(5);
  const [solveTime, setSolveTime] = useState(60);
  const [questionNumber, setQuestionNumber] = useState(1);
  const [currentQuestionNumber, setCurrentQuestionNumber] = useState(1);
  // const quizName = useContext(quizNameContext)

  useEffect(() => {
    const fetchData = async () => {
      try {
        console.log("Value of passQuizName Effect:", passQuizName);
        const createQuizLoadDataResponse = await createQuizLoadData(
          passQuizName
        );
        if (createQuizLoadDataResponse.message === "Quiz Found") {
          console.log(createQuizLoadDataResponse);
          console.log(
            "createQuizLoadDataResponse: " + createQuizLoadDataResponse.message
          );
          setQuestionValue(createQuizLoadDataResponse.quiz.qValue);
          setOptions(
            createQuizLoadDataResponse.quiz.options.map(
              (option) => option.optionValue
            )
          );
          setCorrectOption(
            createQuizLoadDataResponse.quiz.options.findIndex(
              (option) => option.correct
            ) + 1
          );
          setPoints(createQuizLoadDataResponse.quiz.points);
          setSolveTime(createQuizLoadDataResponse.quiz.timeOfQuestion);
          setQuestionNumber(createQuizLoadDataResponse.questionCount);
          setCurrentQuestionNumber(1);
        } else {
          setQuestionValue("");
          setOptions(["", "", "", ""]);
          setCorrectOption(1);
          setPoints(5);
          setSolveTime(60);
          setQuestionNumber(1);
          setCurrentQuestionNumber(1);
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, [passQuizName]);
  console.log("AquestionValue:", questionValue);
  console.log("Aoptions:", options);
  console.log("AcorrectOption:", correctOption);
  console.log("Apoints:", points);
  console.log("AsolveTime:", solveTime);
  console.log("AquestionNumber:", questionNumber);
  console.log("AcurrentQuestionNumber:", currentQuestionNumber);

  const handleOptionChange = (index, value) => {
    const newOptions = [...options];
    newOptions[index] = value;
    setOptions(newOptions);
  };

  const handleSaveQuiz = async () => {
    console.log(passQuizName, currentQuestionNumber);
    const quizData = {
      quizName: passQuizName,
      questionNumber: currentQuestionNumber,
      questionValue: questionValue,
      options: options,
      correctOption: correctOption,
      points: points,
      solveTime: solveTime,
    };
    console.log("Quiz data:", quizData);

    await saveCreateQuiz(quizData);
  };

  const deleteQuizOptions = async () => {
    console.log("Deleting quiz options...");
    const deleteQuizResponse = await deleteQuiz(
      passQuizName,
      currentQuestionNumber
    );
    if (deleteQuizResponse.message === "Delete Sucessfull") {
      window.alert("Delete Sucessfull");
      setQuestionNumber(questionNumber - 1);
      loadQuiz(currentQuestionNumber - 1);
      setQuestionValue("");
      setOptions(["", "", "", ""]);
      setCorrectOption(1);
      setPoints(5);
      setSolveTime(60);
    }
  };

  const addQuiz = () => {
    if (window.confirm("Save Current Quiz First")) {
      setQuestionNumber(questionNumber + 1);
      setCurrentQuestionNumber(currentQuestionNumber + 1);
      setQuestionValue("");
      setOptions(["", "", "", ""]);
      setCorrectOption(1);
      setPoints(5);
      setSolveTime(60);
    }
  };

  const handleLoadQuiz = async (questionNumber) => {
    console.log(`Load quiz ${questionNumber}`);
    const loadQuizResponse = await loadQuiz(passQuizName, questionNumber);
    setCurrentQuestionNumber(questionNumber);
    console.log(loadQuizResponse);
    if (loadQuizResponse.message === "Load Sucessfull") {
      setQuestionValue(loadQuizResponse.question.qValue);
      setOptions(
        loadQuizResponse.question.options.map((option) => option.optionValue)
      );
      setCorrectOption(
        loadQuizResponse.question.options.findIndex(
          (option) => option.correct
        ) + 1
      );
      setPoints(loadQuizResponse.question.points);
      setSolveTime(loadQuizResponse.question.timeOfQuestion);
    } else {
      setQuestionValue("");
      setOptions(["", "", "", ""]);
      setCorrectOption(1);
      setPoints(5);
      setSolveTime(60);
    }
  };

  return (
    <div className="create_quiz_main_area_container">
      <div className="sidebar_container">
        {[...Array(questionNumber)].map((_, index) => (
          <div className="sidebar_box" key={index}>
            Q{index + 1}
            <a href="#" onClick={() => handleLoadQuiz(index + 1)}>
              <img
                src={create_quiz_sidebar_quiz}
                alt={`Create Quiz Sidebar Quiz Image ${index + 1}`}
              />
            </a>
          </div>
        ))}
        <div className="sidebar_add_quiz_button">
          <button className="add_quiz_button" onClick={addQuiz} type="button">
            Add Question
          </button>
        </div>
      </div>
      <div className="create_quiz_main_area_sidebar_seperator">
        <div className="create_quiz_main_area_inner_container">
          <textarea
            placeholder="Enter your Question here"
            className="create_quiz_main_area_textarea"
            value={questionValue}
            onChange={(e) => setQuestionValue(e.target.value)}
          ></textarea>
          <br />
          {options.map((option, index) => (
            <input
              key={index}
              type="text"
              placeholder={`Option ${index + 1}`}
              className="create_quiz_main_area_option"
              value={option}
              onChange={(e) => handleOptionChange(index, e.target.value)}
            />
          ))}
          <div className="create_quiz_data">
            <h6>Provide Answer Option Number:</h6>
            <input
              type="number"
              placeholder="1-4"
              className="create_quiz_main_area_option_correct"
              value={correctOption}
              onChange={(e) => {
                const value = parseInt(e.target.value);
                if (!isNaN(value) && value >= 1 && value <= 4) {
                  setCorrectOption(value);
                }
              }}
            />
          </div>
          <div className="create_quiz_data">
            <h6>Provide Time to Solve in Seconds:</h6>
            <input
              type="number"
              placeholder="60"
              className="create_quiz_main_area_solve_time"
              value={solveTime}
              onChange={(e) => {
                const value = parseInt(e.target.value);
                if (!isNaN(value) && value >= 5) {
                  setSolveTime(value);
                }
              }}
            />
          </div>
        </div>

        <div className="create_quiz_main_area_options">
          <div className="create_quiz_main_area_points_container">
            <label htmlFor="create_quiz_points">Points:</label>
            <input
              type="number"
              placeholder="Points"
              className="create_quiz_main_area_points_input"
              id="create_quiz_points"
              value={points}
              onChange={(e) => setPoints(e.target.value)}
            />
          </div>
          <button
            className="add_quiz_button_delete"
            onClick={deleteQuizOptions}
          >
            Delete
          </button>
          <button className="add_quiz_button_save" onClick={handleSaveQuiz}>
            Save
          </button>
        </div>
      </div>
    </div>
  );
}

export default CreateQuizMainArea;
