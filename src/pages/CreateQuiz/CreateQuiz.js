import React, {useState} from "react";
import "./CreateQuiz.css";
import NavScrollExample from "./Navbar/CreateQuizNavbar";
import CreateQuizMainArea from "./QuizArea/CreateQuizMainArea";
console.log("Creating Quiz JS File");

const CreateQuiz = () => {
  const [passQuizName, setPassQuizName] = useState("");
  // console.log("Value of passQuizName:", passQuizName);
  return (
    <div className="create_quiz_container">
      <div className="create_quiz_navbar">
        <NavScrollExample setPassQuizName={setPassQuizName}/>
      </div>
      <div className="create_quiz_inner_container">
      
      <div className="create_quiz_main_area">
        <CreateQuizMainArea passQuizName={passQuizName}/>
      </div>
      </div>
    </div>
  );
};

export default CreateQuiz;
