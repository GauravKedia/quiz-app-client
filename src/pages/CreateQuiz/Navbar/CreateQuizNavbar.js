    import React, { useState, useEffect } from "react";
    import "bootstrap/dist/css/bootstrap.min.css";
    import DatePicker from "react-datepicker";
    import "react-datepicker/dist/react-datepicker-cssmodules.css";
    import "react-datepicker/dist/react-datepicker.css";
    import { useNavigate } from "react-router-dom";
    import "./CreateQuizNavbar.css";
    import { deployQuiz } from "../../../controller/deployQuiz";
    import { createQuizStartLaunch } from "../../../controller/createQuizStartLaunch";

    // export const quizNameContext = React.createContext();

    const NavScrollExample = ({setPassQuizName}) => {
      const [userName, setUserName] = useState("");
      const [quizName, setQuizName] = useState("");
      const [scheduledDate, setScheduledDate] = useState(null);
      const currentDate = new Date();
      let navigate = useNavigate();

      useEffect(() => {
        const fetchData = async () => {
          try {
            const createQuizStartLaunchResponse = await createQuizStartLaunch();
            setUserName(createQuizStartLaunchResponse.data.userName);
            setQuizName(createQuizStartLaunchResponse.data.quizName);
            setPassQuizName(createQuizStartLaunchResponse.data.quizName);
            console.log("Nav: "+quizName);
          } catch (error) {
            console.error("Error fetching data:", error);
          }
        };

        fetchData();
      }, []);

      const handleDateChange = (date) => {
        setScheduledDate(date);
      };

      const deploy_quiz = async (event) => {
        event.preventDefault();
        if (!scheduledDate) {
          setScheduledDate(new Date());
        }
        await deployQuiz(quizName,scheduledDate);
        let path = `/`;
        navigate(path);
      };

      return (
        // <quizNameContext.Provider value={quizName}>
          <div className="create_quiz_navbar_container">
            <h4 className="create_quiz_navbar_option">Username : {userName}</h4>
            <h4 className="create_quiz_navbar_option">Quiz Title : {quizName}</h4>
            <div className="create_quiz_navbar_schedule">
              <h6>Schedule Quiz:</h6>
              <DatePicker
                id="scheduleQuiz"
                selected={scheduledDate}
                onChange={handleDateChange}
                minDate={currentDate}
                showTimeSelect
                dateFormat="Pp"
                placeholderText="Select schedule date and time"
                className="create_quiz_navbar_option"
              />
            </div>

            <form onSubmit={deploy_quiz} className="create_quiz_launch_quiz">
              <button className="launch_quiz" type="submit">
                Launch Quiz
              </button>
            </form>
          </div>
        // </quizNameContext.Provider>
      );
    };

    export default NavScrollExample;
