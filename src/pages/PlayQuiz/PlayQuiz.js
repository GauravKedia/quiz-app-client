import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import getQuestionData from "../../controller/PlayQuiz/getQuestionData";
import { checkAnswer } from "../../controller/PlayQuiz/checkAnswer";
import Loader from "../loader/loader";
import "./PlayQuiz.css";
const PlayQuiz = () => {
  const navigate = useNavigate();

  const [quizName, setQuizName] = useState("");
  const [question, setQuestion] = useState("");
  const [options, setOptions] = useState([]);
  const [pts, setPts] = useState(0);
  const [countdown, setCountdown] = useState(0);
  const [userName, setUserName] = useState("");
  const [currentScore, setCurrentScore] = useState(0);
  const [currentQNo, setCurrentQNo] = useState(0);
  const [playQuizLoading, setPlayQuizLoading] = useState(false);
  const [isCorrectAnswer, setIsCorrectAnswer] = useState(false);
  const [isWrongAnswer, setIsWrongAnswer] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);
  const [disableSubmitAnswer, setDisableSubmitAnswer] = useState(false);
  const [afterSubmitAnswer, setAfterSubmitAnswer] = useState(null);
  // const [timeToSolve, setTimeToSolve] = useState(countdown);

  useEffect(() => {
    // setPlayQuizLoading(true);
    const fetchData = async () => {
      // Calling Get Question Data
      getQuestionData(
        setQuizName,
        setQuestion,
        setOptions,
        setPts,
        setCountdown,
        setUserName,
        setCurrentScore,
        setCurrentQNo,
        setPlayQuizLoading,
        setDisableSubmitAnswer,
        setAfterSubmitAnswer,
        setIsCorrectAnswer,
        setIsWrongAnswer,
        navigate,
      );
    };

    fetchData();
  }, []);

  useEffect(() => {
    const timer = setInterval(() => {
      setCountdown((prev) => {
        if (prev === 0) {
          clearInterval(timer);
        }
        return prev > 0 ? prev - 1 : prev;
      });
    }, 1000);

    return () => clearInterval(timer);
  }, [countdown]);

  const SubmitAnswer = async () => {
    setAfterSubmitAnswer(selectedOption);
    setPlayQuizLoading(true);
    setDisableSubmitAnswer(true); 
    const checkAnswerResponse = await checkAnswer(
      selectedOption.optionValue,
      currentQNo,
      quizName
    );
    if (checkAnswerResponse === "Correct answer") {
      setIsCorrectAnswer(true);
    } else if (checkAnswerResponse === "Incorrect answer") {
      setIsWrongAnswer(true);
    }
    setPlayQuizLoading(false);
  };

  const handleOptionSelect = async (option) => {
    setSelectedOption(option);
  };

  return (
    <>
      {playQuizLoading ? (
        <Loader />
      ) : (
        <div className="play_quiz_container">
          <div className="play_quiz_navbar">
            <h1 className="play_quiz_title">Quiz App</h1>
            <h1 className="play_quiz_title">{quizName}</h1>
            <div className="play_quiz_user_info">
              <h2>{`Username: ${userName}`}</h2>
              <h2>{`Current Score: ${currentScore}`}</h2>
            </div>
          </div>
          <div className="play_quiz_countdown">
            <div className="play_quiz_countdown_box">
              <p className="play_quiz_countdown_text">{countdown}</p>
            </div>
          </div>
          <div className="play_quiz_main_container">
            <div className="play_quiz_question_container">
              <h1 className="play_quiz_question_title">Question:</h1>
              <div className="play_quiz_question_text">{question}</div>
            </div>
            <div className="play_quiz_options">
              {options.map((option, index) => (
                <div
                  key={index}
                  className={`play_quiz_option 
                  ${selectedOption === option ? "selected" : ""} 
                  `}
                  onClick={() => handleOptionSelect(option)}
                  >
                  {option.optionValue}
                </div>
              ))}
            </div>
            <div className="play_quiz_points">
              <h3>Points:</h3>
              <div className="play_quiz_points_box">{pts}</div>
            </div>
            <div className="play_quiz_actions">
              <button
                className={`play_quiz_submit_button ${disableSubmitAnswer===true ? "turn_submit_off" : ""}`}
                onClick={SubmitAnswer}
                disabled={disableSubmitAnswer}
                >
                Submit Answer
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default PlayQuiz;


// Code if On time correct answer to be shown
// ${
//   afterSubmitAnswer === option && isCorrectAnswer === true
//     ? "correct"
//     : ""
// }
// ${
//   afterSubmitAnswer === option && isWrongAnswer === true
//     ? "wrong"
//     : ""
// }