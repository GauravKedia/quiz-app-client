import React, { useState } from "react";
import { Link,useNavigate } from "react-router-dom";
import {Login} from "../../controller/Auth"

import "./LoginPage.css";

const LoginPage = (props) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [act,setAct] = useState(props.action);
  const navigate = useNavigate();
  const handleUsernameChange = (event) => {
    setUsername(event.target.value);
  };    

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    // console.log(act);
    try{
      console.log(username+ " :: "+password)
      const res = await Login(username,password);
      console.log(res); 
      window.alert(res);
      if(res==="Login Successfull!"){
        if(act==="play"){
          navigate('/play_quiz/initiate');
        }
        else{
        navigate('/create_quiz/initiate');
        }
      }
    }
    catch(error){
      console.log("Login Failed :" +error);
    }
  };

  return (
    <div className="login_page_container">
      <h2>Login</h2>
      <form onSubmit={handleSubmit}>
        <div className="login_form_group">
          <label htmlFor="username" className="login_label">Username</label>
          <input 
            type="text" 
            className="login_form_control" 
            id="login_username" 
            placeholder="Enter username" 
            value={username} 
            onChange={handleUsernameChange} 
          />
        </div>

        <div className="login_form_group">
          <label htmlFor="password" className="login_label">Password</label>
          <input 
            type="password" 
            className="login_form_control" 
            id="login_password" 
            placeholder="Enter password" 
            value={password} 
            onChange={handlePasswordChange} 
          />
        </div>

        <button type="submit" className="btn btn-primary login_button">
          Login
        </button>
      </form>

      <p className="login_p">Don't have an account? <Link to="/auth/register">Register here</Link></p>
    </div>
  );
};

export default LoginPage;
