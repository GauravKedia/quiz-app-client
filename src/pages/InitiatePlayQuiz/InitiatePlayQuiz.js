  import React, { useState } from "react";
  import { useNavigate } from "react-router-dom";
  import { initiatePlayQuiz } from "../../controller/PlayQuiz/InitiatePlayQuiz";
  import "./InitiatePlayQuiz.css";
  import Loader from "../loader/loader";
  import { socket,updateSocketHeaders } from "../../socket";
  import initiateSocket from "../../controller/PlayQuiz/initiateSocket";
  console.log("Loading Join Quiz");

  const InitiatePlayQuiz = () => {
    const [quizName, setQuizName] = useState("");
    const [quizPin, setQuizPin] = useState("");
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();

    
    const handleQuizNameChange = (event) => {
          setQuizName(event.target.value);
    };

    const handleQuizPinChange = (event) => {
      setQuizPin(event.target.value);
    };

    const handleSubmit = async (event) => {
      event.preventDefault();
      try {
        const scheduleResponse = await initiatePlayQuiz(quizName, quizPin);
        if (scheduleResponse && scheduleResponse === "Quiz Access Approved!") {
          setLoading(true);
          await updateSocketHeaders();
          socket.connect();
          initiateSocket(quizName,setLoading,navigate);
        }
        else if(scheduleResponse){
          window.alert(scheduleResponse);
        }
      } catch (error) {
        console.log("Error Occured :" + error);
      }
    };

    return (
      <div className="join_quiz_container">
        {loading ? (
          <Loader />
        ) : (
          <form onSubmit={handleSubmit} className="join_quiz_form_container">
            <input
              name="quiz_name"
              placeholder="Quiz Name"
              autoComplete="off"
              id="quiz_name"
              className="join_quiz_input_container"
              value={quizName}
              onChange={handleQuizNameChange}
            />
            <input
              name="quiz_id"
              placeholder="Quiz PIN"
              autoComplete="off"
              id="quiz_pin"
              className="join_quiz_input_container"
              value={quizPin}
              onChange={handleQuizPinChange}
            />
            <button className="join_quiz_button_container" type="submit">
              Submit
            </button>
          </form>
        )}
      </div>
    );
  };

  export default InitiatePlayQuiz;

