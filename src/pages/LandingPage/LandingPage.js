import React from "react";
import { Link } from "react-router-dom";
import Container from 'react-bootstrap/Container';
import 'bootstrap/dist/css/bootstrap.min.css';

import "./LandingPage.css";

const create_quiz = process.env.PUBLIC_URL + "/images/create_quiz.png";
const join_quiz = process.env.PUBLIC_URL + "/images/join_quiz.png";
console.log("Loading Landing")
const LandingPage = () => {
  return (
    <Container className="landing_page">
      <div className="heading">
        <h1>Quiz App</h1>
      </div>
      <div className="landing_page_buttons">
        <div className="toolbar__buttons">
        <Link to="/auth/login/create" className="toolbar__buttons__create">
            <img src={create_quiz} alt="Create Quiz Image" />
            Create
        </Link>
        <Link to="/auth/login/play" className="toolbar__buttons__join">
            <img src={join_quiz} alt="Create Quiz Image" />
            Join quiz
        </Link>
        </div>
      </div>
    </Container>
  );
};

export default LandingPage;
